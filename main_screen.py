import numpy as np
import cv2
import imutils
import sys


def size():

	leftCam.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*"MJPG"))
	rightCam.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*"MJPG"))
	# leftCam.set(cv2.CAP_PROP_FRAME_WIDTH, mainSizeW)
	# leftCam.set(cv2.CAP_PROP_FRAME_HEIGHT, mainSizeH)
	# rightCam.set(cv2.CAP_PROP_FRAME_WIDTH, mainSizeW)
	# rightCam.set(cv2.CAP_PROP_FRAME_HEIGHT, mainSizeW)


def hsv(frame):
	pass

def blur(frame):
	#gaussianblur(image, (kernel lenght, width), sigma)
	value = cv2.GaussianBlur(frame, (10,10), 1.5
	return value


def canny(frame):
	manualTreshold = 40
	manualEdges = cv2.Canny(blur, 0, manualTreshold)
	autoEdges = imutils.auto_canny(frame)
	return autoEdges


def frame8bit(frame):
	#returns a 8 bit value for input frame
	value = (frame/2**10-1)
	return value.astype('uint8')


def frameIR_toColor(frame):
	pass


def cameras():
	""" function for openning the cameras and seeing them in cv2 gui """
	global leftCam, rightCam, mainSizeL, mainSizeH
    # open the two cameras

    leftCam = c2.VideoCapture(0)
    rightCam = cv2.VideoCapture(1)

    # size of main screen
    mainSizeH = 1280
    mainSizeW = 720

    # size of small screens
    smallSizeH = 640
    smallSizeW = 480

    showWindow = 3
    if cap.isOpened():

    	disp = "display"
        # moguce koristit opengl WINDOW_OPENGL
        cv2.namedWindow(disp, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(disp, mainSizeH, mainSizeW)
        cv2.setWindowTitle(disp, "Stereo Cam")


        while (leftCam.grab() and rightCam.grab()):
        	# check if window is closed on X
            if cv2.getWindowProperty(disp, 0) < 0:
            	break
            # checks if _ is True, then retrieves frame from cameras
            _, frameLeft = leftCam.retrieve()
            _, frameRight= rightCam.retrieve()

            if showWindow == 3:
            	frameLeftRs = cv2.resize(frame8bit(frameLeft), smallSizeW, smallSizeH)
            	frameRightRs = cv2.resize(frame8bit(frameRight), smallSizeW, smallSizeH)
            	blurRs = cv2.resize(blur, (640, 360))
                edgesRs = cv2.resize(edges, (640, 360))

            	vidBuf = np.concatenate(
                    (frameRs, cv2.cvtColor(hsvRs, cv2.COLOR_GRAY2BGR)), axis=1)
               
                vidBuf1 = np.concatenate((cv2.cvtColor(blurRs, cv2.COLOR_GRAY2BGR), cv2.cvtColor(
                    edgesRs, cv2.COLOR_GRAY2BGR)), axis=1)
                vidBuf = np.concatenate((vidBuf, vidBuf1), axis=0)

            if showWindow == 1:  # Show Camera Frame Left
                displayBuf = frame

            elif showWindow == 2:  # Show Camera Frame Right
                displayBuf = edges

            elif showWindow == 3:  # Show Canny Left
                displayBuf = vidBuf

            elif showWindow == 3:  # Show Canny Right
                displayBuf = vidBuf1

            elif showWindow == 3:  # Show All Stages
                displayBuf = vidBuf1

            elif showWindow == 3:  # Show Color Left
                displayBuf = vidBuf    

           	elif showWindow == 3:  # Show Color Right
                displayBuf = vidBuf

            elif showWindow == 3:  # Show Stereo Color
                displayBuf = vidBuf

            elif showWindow == 3:  # Show Disparty
                displayBuf = vidBuf

           	0v2.imshow(windowName, displayBuf)
            key = cv2.waitKey(10)
            if key == 27:  # Check for ESC key
                cv2.destroyAllWindows()
                break
            elif key == 49:  # 1 key, show frame
                cv2.setWindowTitle(windowName, "Camera Feed")
                showWindow = 1
            elif key == 50:  # 2 key, show Canny
                cv2.setWindowTitle(windowName, "Canny Edge Detection")
                showWindow = 2
            elif key == 51:  # 3 key, show Stages
                cv2.setWindowTitle(
                    windowName, "Camera, Gray scale, Gaussian Blur, Canny Edge Detection")
                showWindow = 3
            elif key == 52:  # 4 key, toggle help
                showHelp = not showHelp
            elif key == 44:  # , lower canny edge threshold
                edgeThreshold = max(0, edgeThreshold-1)
                print('Canny Edge Threshold Maximum: ', edgeThreshold)
            elif key == 46:  # , raise canny edge threshold
                edgeThreshold = edgeThreshold+1
                print('Canny Edge Threshold Maximum: ', edgeThreshold)
            elif key == 74:  # Toggle fullscreen; This is the F3 key on this particular keyboard
                # Toggle full screen mode
                if showFullScreen == False:
                    cv2.setWindowProperty(
                        windowName, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
                else:
                    cv2.setWindowProperty(
                        windowName, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_NORMAL)
                showFullScreen = not showFullScreen




    else:
    	print ("No video feed")


if __name__=='__main__':
	read_cam()